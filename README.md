# Resume
Generates html, pdf and docx from a single markdown file.
- html / docx: built using pandoc
- pdf: built using pandoc (tex) and context

## Usage
1. Install dependencies:
    - `pandoc`
    - `texlive-context`
2. Build:
    ```
    ./build
    ```

## Notes
- [Google Fonts' version of Raleway](https://fonts.google.com/specimen/Raleway) raises a Lua (5.3.6) error
  - Use other static source instead (ex: [FontSquirrel](https://www.fontsquirrel.com/fonts/raleway))
