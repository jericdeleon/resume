# Jeric de Leon

*hire@jericdeleon.com &bull; 657 210 0260 &bull; Laurel, MD &bull; [profile](https://jericdeleon.gitlab.io) &bull; [html](https://jericdeleon.gitlab.io/resume) &bull; [pdf](https://jericdeleon.gitlab.io/resume/resume.pdf) &bull; [docx](https://jericdeleon.gitlab.io/resume/resume.docx)*

---

\startlines
\stoplines

Hi GitHub Team,

\startlines
\stoplines

I am a Full Stack Developer looking out for my next role, and I would like to try and see if I would be a great fit with GitHub!

\startlines
\stoplines

One of the top highlights that attracted me to GitHub was the emphasis on kindness and understanding. I exercise the belief that a lot of the problems out there can be navigated a lot more simpler by just putting ourselves in the shoes of the other party, understanding their struggle and calmly working with them through it. If a place has psychological safety, where colleagues and clients can warmly cooperate without friction, then the best outcome will always happen, and I will gladly be the one to initiate this atmosphere (and keep it that way). My approach to people is the same as the company's, where trust is the default. Fault finding and finger pointing is a big no in my book (I don't like `git blame` being named as such, for one; it could have been named something else), but when I make mistakes, I fully own it not just for responsibility but also to shield my team mates from being blamed incorrectly.

\startlines
\stoplines

On the technical side, I have a total of about 5 years experience in Full Stack Development: 4 years in a Rails / Angular stack, and 1 year in an Angular / Nativescript / Firebase stack. It was both challenging and fun to find the most pragmatic solution to multi-layer problems like these, and I usually take my time to go over multiple approaches to a problem and weigh each tradeoff, whether it be choosing a dependency, reorganizing the architecture, or changing the API flow. I believe that hard, critical thinking during the design phase of a problem to make it resilient and highly available often saves a lot of extra work that can emerge from a good-for-now solution, and I would be glad to continue this generalist kind of work in GitHub.

\startlines
\stoplines

I have effectively operated remotely (in phases) with my respective teams for my last two roles, and deeply understand the importance of great asynchronous documentation, so I do my best in documenting everything: not just developer guides for navigating the application code base, or user guides for clients and testers, but even things like my line of reasoning when laying the data model design for the next feature, or my thought process on an ongoing issue, or every step that I make when chasing a production bug. My mindset is: any person going blind into whatever I was working on should also be able to read my "intent" as quick as possible.

\startlines
\stoplines

I have been using GitLab for the last 5 years primarily because back then, they were first to offer free private repos to non-premium users, and also due to GitLab CI. But ever since GitHub started tackling the competition with GitHub Actions, I've always been interested if the parity gap in other features can be solved, and I would be glad to be a part of that.

\startlines
\stoplines

I see a lot of open positions on the site, and because of my generalist mindset, I'm not sure which to apply for, but these roles would be a good set of initial targets for me:

- Software Engineer - Core Products
- Software Engineer - Platform
- Software Engineer - App Core
- Software Engineer - Special Projects
- Software Engineer - Code to Cloud

\startlines
\stoplines

I hope I was able to introduce a bit of myself, and I certainly look forward that I will get a chance to proceed. If you see me as a great fit, then I will try my best in the process!

\startlines
\stoplines

Sincerely,

\startlines
\stoplines

Jeric de Leon

\break

**Why do you want to work at GitHub?**

\startlines
\stoplines

I believe that GitHub's values overlap a lot with those I hold in high value: understanding first and foremost, trust by default, and taking full ownership on the outcome of your work.

\startlines
\stoplines

**Why do you feel you would be a fit for this role?**

\startlines
\stoplines

I believe I can bring value by the combination of my ff. experiences: the sense of careful oversight I have gained as we guided not just a Rails monolith (along with a fork of Spree, another Rails monolith), but also an Angular frontend, from conceptualization to maintenance; the humility that tech debt should be managed just as equally important as a roadmap feature; and my remote ethic.

\startlines
\stoplines
**What do you think are some of the challenges of working remotely, and how would you address them?**

\startlines
\stoplines

I see two main challenges on working remotely: reaching consensus, and relaying your thought process. Consensus is easy when one side is heavily favored by the majority, but when the pros and cons are equal, I take my time to determine from each participant on how they arrived at their choice, and also ask guidance from experts on showing concerns that haven't been illuminated yet. I tackle asynchronous communication by writing and documenting almost everything I go through: the problems currently being seen, the paths available, the tradeoffs, itemized actionable items for both proof-of-concept and final approach tasks, as well as every consideration encountered and chosen at every step.

\startlines
\stoplines

**Tell us about a cool program or project you’ve used or seen that takes both infrastructure design and software engineering concepts into consideration. What is it and why did you think it was really interesting/cool?**
\startlines
\stoplines

I have always been fascinated in container orchestration solutions, like Kubernetes / Docker Swarm / Nomad + Consul, and how it manages to achieve multi-node communication and availability. I remember my undergraduate self, struggling to think of the easiest, ideal solution to handle command of a fleet of servers with multiple application stacks, when containers were still an unknown concept. As I read on it, I realized that just like software engineers in remote work, one of the key pieces driving it is the same: consensus and gossip algorithms.

\startlines
\stoplines

**Tell us about an application that you helped to develop and maintain that made it to production. What were some scale and system-related considerations you had to work through?**

\startlines
\stoplines

While designing our last stack, I wanted the servers to scale easily. We could have just used Heroku, and let them take care of scaling for us, but I didn't want the vendor lock-in to prevent growth and the possibility of managing everything on-prem. I did want a container-based solution, so I can take advantage of an orchestrator, but Kubernetes would have demanded too much focus, which led to to Docker Swarm as the most pragmatic solution. To pave the way to make our application container-ready, lessons were learned: server state like session stores, cache stores, and jobs were moved from memory to Redis. Caches and SQL tuning had to be taken advantage of to minimize database calls.

\startlines
\stoplines

**Pick a feature or screen of a developer tool you use and tell us how you'd make it better. What problems do you see with the current approach? What challenges do you anticipate in implementing the improvement? For example, you could choose your favorite editor, git client, or GitHub itself.**

\startlines
\stoplines

I am currently in love with vim, but there is one VS Code feature that I truly want to be available in vim: VS Code's remote development extension pack, which allows code in the editor to be executed in a container locally or remote. I figured that if I'm gonna take a stab at it, on a high level I'll need to: learn Vimscript for the plugin itself, manage the SSH connection to the remote machine, find out if piping normal Docker commands would be enough to fire up the container on the target system or if something else would be more apt, sync both ends together, and find the best way to make the experience reliable. If that single config file can be used both in VS Code and in vim, I'll be a happy kid.

\startlines
\stoplines

**Describe a time when you advocated for a change that was important to the community or your team. What was it? Why was it so important? How did you approach the subject with your team? Was it successful? Why or why not?**

\startlines
\stoplines

When our application was at its earliest stages, each build had to be done manually and the server had to be updated manually. To solve the problem, I advocated for a tag-based CI/CD workflow in our workplace, and once I relayed to my director the idea that just creating a git tag would be enough to deploy, the value was easily seen and he let me go ahead with it. We still use it to this day, and every time I use it I feel proud in having a hand on creating a solution that saved hours of work for the entire team.
