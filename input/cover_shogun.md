# Jeric de Leon

*hire@jericdeleon.com &bull; 657 210 0260 &bull; Laurel, MD &bull; [profile](https://jericdeleon.gitlab.io) &bull; [html](https://jericdeleon.gitlab.io/resume) &bull; [pdf](https://jericdeleon.gitlab.io/resume/resume.pdf) &bull; [docx](https://jericdeleon.gitlab.io/resume/resume.docx)*

---

\startlines
\stoplines

Hi Shogun Team,

\startlines
\stoplines

I am a Full Stack Developer looking out for my next role, and I would like to try and see if I would be a great fit as a Rails Engineer with Shogun!

\startlines
\stoplines

One of top criterias that attracted me to Shogun was a personal alignment to the core values: understanding and empathy, continuous growth and a preference to do something right rather than doing something easy. The team's approach to asynchronous, remote communication, where clear documentation and constant sharing of intent is key, is also a trait I have always applied to my existing work, and would be glad to retain this kind of culture in Shogun.

\startlines
\stoplines

On the technical side, I have a total of about 5 years experience in Full Stack Development: 4 years in a Rails / Angular stack, and 1 year in an Angular / Nativescript / Firebase stack. On the Rails side, we have chosen to use Spree as our base for the checkout system. It's not Shopify or BigCommerce, but it has been quite an adventure from knowing nothing about it, to customizing almost everything within it: I have added changes to our fork on the checkout state, shipment consolidation logic, payment gateway API additions, custom city-level taxation, and an entire workorder system using the line item as the bridge. With a Rails-specific position, I'll be excited to polish my Rails proficiency with top-of-line best practices and also, hopefully, slowly transition my Angular knowledge to React in a professional environment.

\startlines
\stoplines

I have an attraction for eliminating repeated work and pain points, and do my fair share of these kinds of tasks when possible: In my current role, I was responsible for advocating and executing a tag-based CI/CD workflow, where we only have to create a tag on a release branch and a build/test/deploy flow will automatically be triggered. It's still reliably used today, and it inspires me if I can solve similar problems that can save tons of hours for everyone else.

\startlines
\stoplines

I hope I was able to introduce a bit of myself, and I certainly look forward that I will get a chance to proceed. If you see me as a great fit, then I will try my best in the process!

\startlines
\stoplines

Sincerely,

\startlines
\stoplines

Jeric de Leon
