# Jeric de Leon

*hire@jericdeleon.com &bull; 657 210 0260 &bull; Laurel, MD &bull; [profile](https://jericdeleon.gitlab.io) &bull; [html](https://jericdeleon.gitlab.io/resume) &bull; [pdf](https://jericdeleon.gitlab.io/resume/resume.pdf) &bull; [docx](https://jericdeleon.gitlab.io/resume/resume.docx)*

---

I am a generalist software engineer (client-side, server-side, mobile, architecture, DevOps, system administration). I am deliberate by default, and place high priority on careful design, pragmatism, evaluation and execution. My broad skillset is the product of my desire to learn, hone and optimize the best-fit architecture, language and tool for the areas I own.

---

## History

**2017**
: **Senior Software Engineer**, Dentca Inc.

    - Led the architecture, development and maintenance of Dentca's web platform for dentists to design, order and 3D-print dentures (using WebGL), and for production to process work orders / materials
    - Designed a scalable, multi-tiered stack (Angular frontend, Rails backend, Sidekiq jobs) for multiple systems (3 separate app systems, 3 environments each) on a container orchestrator (Docker Swarm) with automatic SSL renewal (Traefik), managed through automated, tag-driven CI deployment
    - Reliably used Reactive Functional Programming (RxJS, NgRx) for state and reactivity
    - Implemented real-time messaging and updates (WebSockets), checkout processing (Spree), credit card processing (Authorize.NET), internationalization (I18N)
    - Managed team issues from triage, replication, investigation, fix, testing, QA, to deployment confirmation
    - Tech: *AWS (EC2, Elasticache, IAM, RDS, S3, VPC), Angular, Ansible, Bash, Docker Swarm, Gitlab CI, HTML, ImmutableJS, Javascript, NgRx, Nginx, Postgres, Rails, Redis, Ruby, RxJS, Sidekiq, Spree, SQL, Traefik, Typescript, Unix*

**2016 - 2017**
: **Lead Mobile / Web Developer**, Freelance

    - Led development of #SafelyPH, a cross-platform mobile app with a web-based admin panel for locating nearby social hygiene clinics in the Philippines for Save The Children
    - Tech: *Angular, Clarity UI, Firebase, NativeScript, RxJS*

**2016 - 2017**
: **Web / Desktop Developer**, Deutsche Knowledge Services

    - Implemented broad features (UI enhancements, data transformations, API migrations) and security improvements (SSL, End-to-end Encryption, Single Sign-On) for multiple in-house applications
    - Tech: *.NET MVC, .NET WebAPI, C#, Entity Framework, Hibernate, Java, Oracle, SQL Server, Spring*

**2014 - Hold**
: **MS Computer Science**, University of the Philippines

    - Built an SVM-based facial feature detector using *R*

**2013 - 2015**
: **Mainframe Developer**, JP Morgan

    - Improved the throughput of a security reconciliation system by transitioning an RDBMS-based subsystem to Message Queues, drastically eliminating incidents of DB row lock issues
    - Tech: *CICS, COBOL, DB2, IBM MQ, JCL*

**2008 - 2013**
: **BS Industrial Engineering**, University of the Philippines

---

## Skills

- **Knowledgeable**: *AWS (EC2, Elasticache, IAM, RDS, S3, VPC), Angular, Ansible, Bash, Digital Ocean, Docker Swarm, Firebase, HTML, ImmutableJS, Javascript, MongoDB, NativeScript, NgRx, Nginx, Postgres, Rails, Redis, Redux, Ruby, RxJS, Sidekiq, Spree, SQL, Traefik, Typescript, Unix*
- **Excited About**: *Blockchain, CQRS, Elixir, Functional Programming, Go, GraphQL, Haskell, Kubernetes, React, Rust, System Design, Terraform, Vimscript*
